package sample;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import sample.objects.Polygon;

public class Main {

    private static Polygon mozgolomicModel;
    private static List<Polygon> mozgolomicPolygons;
    private static Polygon testModel;
    private static List<Polygon> testPolygons;

    static {
        mozgolomicModel = new Polygon(90, 90, 135, 135, 135, 135, 135, 135, -90);
        mozgolomicPolygons = Arrays.asList(
                new Polygon(135, 135, 45, 45),
                new Polygon(135, 90, 135, 90, 90),
                new Polygon(45, 90, 45),
                new Polygon(90, 45, 135, 90),
                new Polygon(90, 45, 135, 90),
                new Polygon(135, 90, 45, 90),
                new Polygon(90, 135, 135, 90, 90),
                new Polygon(90, 135, 90, 45),
                new Polygon(45, 90, 90, 135)
        );
        testModel = new Polygon(90, 45, -90, 45, 90);
        testPolygons = Arrays.asList(
                new Polygon(45, 45, 90),
                new Polygon(45, 45, 90),
                new Polygon(45, 45, 90)
        );
    }

    public static void main(String[] args) {
        
    }

    public static List<Polygon> merge(Polygon polygon1, Polygon polygon2) {
        return Collections.emptyList();
    }
}
