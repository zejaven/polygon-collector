package sample.objects;

import java.util.Arrays;
import java.util.List;

public class Polygon {
    
    private List<Integer> angles;
    private List<Polygon> polygons;

    public Polygon(Integer... angles) {
        this.angles = Arrays.asList(angles);
    }

    public List<Integer> getAngles() {
        return angles;
    }

    public List<Polygon> getPolygons() {
        return polygons;
    }

    public void setPolygons(List<Polygon> polygons) {
        this.polygons = polygons;
    }
}
